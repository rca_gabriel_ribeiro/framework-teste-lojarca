import Header from '../../materials/components/header/header.js';
import Slider from '../../materials/components/slider/slider.js';

class App {
	constructor() {

		new Header();
		new Slider();

	}
}

export default App;
